﻿using Autofac;
using NodeTree.Data;
using NodeTree.Interfaces;
using NodeTree.Services;
using NodeTreeTests.Test.Helpers;
using NUnit.Framework;
using System.IO;
using System.Threading.Tasks;

namespace NodeTreeTests.Test.IntegrationTests
{
    [TestFixture]
    public class NodeWriterTest
    {
        private IContainer Container { get; set; }
        private ContainerBuilder Builder { get; set; }

        [SetUp]
        public void Setup()
        {
            // arrange

            Builder = new ContainerBuilder();
            Builder.RegisterType<NodeTransformer>().As<INodeTransformer>();
            Builder.RegisterType<NodeWriter>().As<INodeWriter>();
        }

        [Test]
        public async Task NodeWriter_WithInjectedProductionDescriber_ShouldWriteTreeToFile()
        {
            // arrange

            Builder.RegisterType<NodeDescriber>().As<INodeDescriber>();
            Container = Builder.Build();

            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<INodeWriter>();

                var testData = new SingleChildNode("root",
                    new TwoChildrenNode("child1",
                        new ManyChildrenNode("leaf1"),
                        new SingleChildNode("child2",
                            new SingleChildNode("leaf2"))));
                // act
                await writer.WriteToFileAsync(testData, "TestFile.txt");
            }

            var expectedResult =

@"new SingleChildNode(""root"",
    new TwoChildrenNode(""child1"",
        new NoChildrenNode(""leaf1""),
        new SingleChildNode(""child2"",
            new NoChildrenNode(""leaf2""))))
";
            // assert
            Assert.AreEqual(expectedResult, File.ReadAllText("TestFile.txt"));
        }

        [Test]
        public async Task NodeWriter_WithInjectedMockDescriber_ShouldWriteMockTextToFile()
        {
            // arrange

            Builder.RegisterType<MockDescriber>().As<INodeDescriber>();
            Container = Builder.Build();

            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<INodeWriter>();

                var testData = new SingleChildNode("root",
                    new TwoChildrenNode("child1",
                        new ManyChildrenNode("leaf1"),
                        new SingleChildNode("child2",
                            new SingleChildNode("leaf2"))));
                // act
                await writer.WriteToFileAsync(testData, "TestFile.txt");
            }

            var expectedResult =

@"I am the mock describer, i have been injected using Autofac!";

            // assert
            Assert.AreEqual(expectedResult, File.ReadAllText("TestFile.txt"));
        }
    }
}
