﻿using NodeTree.Data;
using NodeTree.Interfaces;
using NodeTree.Services;
using NUnit.Framework;

namespace NodeTreeTests.Test.UnitTests
{
    [TestFixture]
    public class DescriberTests
    {
        [Test]
        public void NodeDescriber_WithNoChildrenNode_ShouldReturnTreeString()
        {
            // arrange

            var testData = new NoChildrenNode("root");

            INodeDescriber describer = new NodeDescriber();

            // act
            var resultString = describer.Describe(testData);

            // assert
            Assert.AreEqual(resultString,

@"new NoChildrenNode(""root"")
");
        }

        [Test]
        public void NodeDescriber_WithNodeIncludingSubtree_ShouldReturnTreeString()
        {
            // arrange

            var testData = new SingleChildNode("root",
                new SingleChildNode("child1",
                    new NoChildrenNode("leaf1")));

            INodeDescriber describer = new NodeDescriber();

            // act
            var resultString = describer.Describe(testData);

            // assert
            Assert.AreEqual(resultString,

@"new SingleChildNode(""root"",
    new SingleChildNode(""child1"",
        new NoChildrenNode(""leaf1"")))
");
        }

        [Test]
        public void NodeDescriber_WithTreeNodesAndUnusedSlots_ShouldReturnTreeString()
        {
            // arrange

            var testData = new ManyChildrenNode("root",
                new ManyChildrenNode("child1",
                    new ManyChildrenNode("leaf1")));

            INodeDescriber describer = new NodeDescriber();

            // act
            var resultString = describer.Describe(testData);

            // assert
            Assert.AreEqual(resultString,

@"new ManyChildrenNode(""root"",
    new ManyChildrenNode(""child1"",
        new ManyChildrenNode(""leaf1"")))
");
        }

        [Test]
        public void Task1_WithProvidedSampleData_ShouldReturnTreeString()
        {
            // arrange

            var testData = new SingleChildNode("root",
                new TwoChildrenNode("child1",
                    new NoChildrenNode("leaf1"),
                    new SingleChildNode("child2",
                        new NoChildrenNode("leaf2"))));

            INodeDescriber describer = new NodeDescriber();

            // act
            var resultString = describer.Describe(testData);

            // assert
            Assert.AreEqual(resultString,

@"new SingleChildNode(""root"",
    new TwoChildrenNode(""child1"",
        new NoChildrenNode(""leaf1""),
        new SingleChildNode(""child2"",
            new NoChildrenNode(""leaf2""))))
");
        }
    }
}