﻿using NodeTree.Common;
using NodeTree.Data;
using NUnit.Framework;
using System.Linq;

namespace NodeTreeTests.Test.UnitTests
{
    /// <summary>
    /// A class to test the traversal method used in describer and transformer
    /// </summary>
    [TestFixture]
    public class UtilityTests
    {
        [Test]
        public void TraverseTreeDepthFirst_WithTree_ShouldReturnNodesWithChildrenUnderParents()
        {
            // arrange

            var testdata = new ManyChildrenNode("Root",
                new TwoChildrenNode(" Root/A",
                    new TwoChildrenNode(" A/1",
                        new NoChildrenNode(" 1/a"),
                        new NoChildrenNode(" 1/b")),
                    new NoChildrenNode(" A/2")),
                new NoChildrenNode(" Root/B"),
                new TwoChildrenNode(" Root/C",
                    new NoChildrenNode(" C/1"),
                    new NoChildrenNode(" C/2")));
            var resultString = "";

            // act
            var sut = Utility.TraverseTreeDepthFirst(testdata);
            foreach (var item in sut.Select(x => x.Node.Name).ToArray())
            {
                resultString += item;
            }

            // assert
            Assert.AreEqual(resultString, "Root Root/A A/1 1/a 1/b A/2 Root/B Root/C C/1 C/2");
        }
    }
}