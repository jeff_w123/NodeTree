﻿using NodeTree.Data;
using NodeTree.Interfaces;
using NodeTree.Services;
using NodeTreeTests.Test.Helpers;
using NUnit.Framework;

namespace NodeTreeTests.Test.UnitTests
{
    [TestFixture]
    public class TransformerTests
    {
        [Test]
        public void Transformer_WithTreeAndAllNodeSlotsUsed_ShouldReturnSameNodeTypes()
        {
            // arrange

            var testData = new ManyChildrenNode("root",
                new NoChildrenNode("child1"),
                new NoChildrenNode("child2"),
                new NoChildrenNode("Child3"));

            INodeTransformer transformer = new NodeTransformer();

            // act
            var sut = transformer.Transform(testData);
            var resultString = TestHelper.GetTypeNamesFromNode(sut);

            // assert
            Assert.AreEqual(resultString,

@" ManyChildrenNode NoChildrenNode NoChildrenNode NoChildrenNode");

        }

        [Test]
        public void Transformer_WithTreeAndUnusedSlots_ShouldReturnUpdatedNodeTypes()
        {
            // arrange

            var testData = new TwoChildrenNode("root",
                new NoChildrenNode("child1"),
                new ManyChildrenNode("Child2 - should convert to NoChildrenNode"));

            INodeTransformer transformer = new NodeTransformer();

            // act
            var sut = transformer.Transform(testData);
            var resultString = TestHelper.GetTypeNamesFromNode(sut);

            // assert
            Assert.AreEqual(resultString,

@" TwoChildrenNode NoChildrenNode NoChildrenNode");

        }

        [Test]
        public void Task2_WithProvidedSampleDataAndProductionCode_ShouldReturnUpdatedTree()
        {
            // arrange

            var testData = new ManyChildrenNode("root",
                new ManyChildrenNode("child1",
                    new ManyChildrenNode("leaf1"),
                    new ManyChildrenNode("child2",
                        new ManyChildrenNode("leaf2"))));

            INodeTransformer transformer = new NodeTransformer();
            INodeDescriber describer = new NodeDescriber();

            // act
            var sut = transformer.Transform(testData);
            var resultString = describer.Describe(sut);

            // assert
            Assert.AreEqual(resultString,

@"new SingleChildNode(""root"",
    new TwoChildrenNode(""child1"",
        new NoChildrenNode(""leaf1""),
        new SingleChildNode(""child2"",
            new NoChildrenNode(""leaf2""))))
");
        }
    }
}
