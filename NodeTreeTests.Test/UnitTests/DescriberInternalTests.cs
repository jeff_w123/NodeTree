﻿using NodeTree.Common;
using NodeTree.Data;
using NodeTree.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace NodeTreeTests.Test.UnitTests
{
    /// <summary>
    /// A class to test the encapsulated methods in NodeDescriber
    /// </summary>
    [TestFixture]
    public class DescriberInternalTests : NodeDescriber
    {

        [TestCase("A", 0)]
        [TestCase("B", 1)]
        [TestCase("E", 3)]
        [TestCase("J", 2)]
        [TestCase("F", 2)]
        public void SetTreeLevels_WithTree_ShouldCalculateNodeLevels(string nodeName, int treeLevel)
        {
            // arrange

            var testdata = new ManyChildrenNode("A",
                new TwoChildrenNode("B",
                    new TwoChildrenNode("C",
                        new NoChildrenNode("D"),
                        new NoChildrenNode("E")),
                    new NoChildrenNode("F")),
                new NoChildrenNode("G"),
                new TwoChildrenNode("H",
                    new NoChildrenNode("I"),
                    new NoChildrenNode("J")));
            var sut = Utility.TraverseTreeDepthFirst(testdata);

            // act
            SetTreeLevels(sut);

            // assert
            Assert.True(sut.FirstOrDefault(x => x.Node.Name == nodeName).TreeLevel == treeLevel);
        }

        [TestCase("A", 0)]
        [TestCase("D", 1)]
        [TestCase("E", 2)]
        [TestCase("H", 0)]
        [TestCase("J", 3)]
        public void SetClosingBrackets_WithTree_ShouldCalculateNoOfBrackets(string nodeName, int noOfBrackets)
        {
            // arrange

            var testdata = new ManyChildrenNode("A",
                new TwoChildrenNode("B",
                    new TwoChildrenNode("C",
                        new NoChildrenNode("D"),
                        new NoChildrenNode("E")),
                    new NoChildrenNode("F")),
                new NoChildrenNode("G"),
                new TwoChildrenNode("H",
                    new NoChildrenNode("I"),
                    new NoChildrenNode("J")));
            var sut = Helpers.TestHelper.TraverseTreeDepthFirst(testdata);

            // act
            SetClosingBrackets(sut);

            // assert
            Assert.True(sut.FirstOrDefault(x => x.Node.Name == nodeName).NoOfBrackets == noOfBrackets);
        }

        public static IEnumerable<TestCaseData> TestsForCorrectIndentation
        {
            get
            {
                // arrange
                yield return new TestCaseData(new NodeItem(new NoChildrenNode("A")), 0)
                    .Returns("new NoChildrenNode(\"A\",\n");
                yield return new TestCaseData(new NodeItem(new NoChildrenNode("B")), 3)
                    .Returns("            new NoChildrenNode(\"B\",\n");
            }
        }

        [Test, TestCaseSource(typeof(DescriberInternalTests), "TestsForCorrectIndentation")]

        public string PrintNodeLine_WithNodeItem_ShouldReturnCorrectIndentation(NodeItem nodeItem, int treeLevel)
        {
            // arrange
            nodeItem.TreeLevel = treeLevel;

            // act
            var sut = PrintNodeLine(nodeItem);

            // assert
            return sut;
        }

        public static IEnumerable<TestCaseData> TestsForLineEnding
        {
            get
            {
                // arrange
                yield return new TestCaseData(new NodeItem(new NoChildrenNode("A")) { NoOfBrackets = 0 }, false)
                    .Returns("new NoChildrenNode(\"A\",\n");
                yield return new TestCaseData(new NodeItem(new NoChildrenNode("B")) { NoOfBrackets = 1}, false)
                    .Returns("new NoChildrenNode(\"B\"),\n");
                yield return new TestCaseData(new NodeItem(new NoChildrenNode("C")) { NoOfBrackets = 2}, true)
                    .Returns("new NoChildrenNode(\"C\"))\n");
            }
        }

        [Test, TestCaseSource(typeof(DescriberInternalTests), "TestsForLineEnding")]
        public string PrintNodeLine_WithNodeItem_ShouldReturnCorrectLineEnding(NodeItem nodeItem, bool isAtEndOfList)
        {
            // act
            var sut = PrintNodeLine(nodeItem, isAtEndOfList);

            // assert
            return sut;
        }
    }
}