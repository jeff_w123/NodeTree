﻿using NodeTree.Data;
using NodeTree.Interfaces;
using NodeTree.Services;
using NodeTreeTests.Test.Helpers;
using NUnit.Framework;
using System.Collections.Generic;

namespace NodeTreeTests.Test.UnitTests
{
    /// <summary>
    /// A class to test the encapsulated methods in NodeTransformer
    /// </summary>
    [TestFixture]
    public class TransformerInternalTests : NodeTransformer
    {
        [Test]
        public void BuildTree_WithNodeItemList_ShouldCreateTree()
        {
            // arrange - create Nodes, NodeItems and set ParentNodes

            var nodeA = new TwoChildrenNode("A");
            var nodeB = new TwoChildrenNode("B");
            var nodeC = new TwoChildrenNode("C");
            var A = new NodeItem(nodeA) { ParentNode = null };
            var B = new NodeItem(nodeB) { ParentNode = nodeA };
            var C = new NodeItem(nodeC) { ParentNode = nodeB };

            // act
            var sut = BuildTree(new List<NodeItem>() { A, B, C} );
            var rebuiltTreeNames = TestHelper.GetNodeNames(sut);

            // assert
            Assert.AreEqual("ABC", rebuiltTreeNames);
        }

        [Test]
        public void Transformer_WithFilledSlotNodeItems_ShouldReturnSame()
        {
            // arrange

            var nodeA = new SingleChildNode("A");
            var nodeB = new NoChildrenNode("B");
            var A = new NodeItem(nodeA) { ParentNode = null };
            var B = new NodeItem(nodeB) { ParentNode = nodeA };
            var originalNode = BuildTree(new List<NodeItem>() { A, B });

            // get type names for comparison
            var originalNodeTypeNames = TestHelper.GetTypeNamesFromNode(originalNode);
            INodeTransformer transformer = new NodeTransformer();

            // act
            var sut = transformer.Transform(originalNode);
            var result = TestHelper.GetTypeNamesFromNode(originalNode);

            // assert
            Assert.AreEqual(" SingleChildNode NoChildrenNode", originalNodeTypeNames);
            Assert.AreEqual(" SingleChildNode NoChildrenNode", result);
        }

        [Test]
        public void Transformer_WithSpareSlotNodeItems_ShouldReturnUpdated()
        {
            // arrange

            var nodeA = new TwoChildrenNode("A - Should become SingleChildNode");
            var nodeB = new SingleChildNode("B - Edge node, should become NoChildrenNode");
            var A = new NodeItem(nodeA) { ParentNode = null };
            var B = new NodeItem(nodeB) { ParentNode = nodeA };
            var lists = new List<NodeItem>() { A, B };
            var originalNode = BuildTree(new List<NodeItem>() { A, B });
        
            // get type names for comparison
            var originalNodeTypeNames = TestHelper.GetTypeNamesFromNode(originalNode);
            INodeTransformer transformer = new NodeTransformer();

            // act
            var sut = transformer.Transform(originalNode);
            var result = TestHelper.GetTypeNamesFromNode(sut);

            // assert
            Assert.AreEqual(" TwoChildrenNode SingleChildNode", originalNodeTypeNames);
            Assert.AreEqual(" SingleChildNode NoChildrenNode", result);
        }
    }
}
