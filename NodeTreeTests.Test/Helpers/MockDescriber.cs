﻿using NodeTree.Interfaces;
using NodeTree.Data;

namespace NodeTreeTests.Test.Helpers
{
    /// <summary>
    /// A class to mock the node describer.
    /// </summary>

    public class MockDescriber : INodeDescriber
    {
        public string Describe(Node node)
        {
            // Ignore node!
            return "I am the mock describer, i have been injected using Autofac!";
        }
    }
}
