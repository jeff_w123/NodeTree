﻿using NodeTree;
using NodeTree.Data;
using System.Collections.Generic;
using System.Linq;

namespace NodeTreeTests.Test.Helpers
{
    /// <summary>
    /// Some static methods for use in tests.
    /// </summary>
    public static class TestHelper
    {
        /// <summary>
        /// A method to traverse the tree and get the node names, used for testing.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>

        public static string GetNodeNames(Node node)
        {
            var finalList = "";
            var nodeStack = new Stack<Node>();
            nodeStack.Push(node);

            while (nodeStack.Count > 0)
            {
                var nextNode = nodeStack.Pop();

                if (nextNode != null)
                {
                    foreach (var child in nextNode.GetChildrenNodes().Reverse())
                    {
                        nodeStack.Push(child);
                    }
                    finalList += nextNode.Name; 
                }
            }
            return finalList;
        }

        /// <summary>
        /// A method to iterate a nodeItem list and get the node names, used for testing.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static string GetNodeNamesFromList(List<NodeItem> nodeItems)
        {
            var finalList = "";

            foreach (var item in nodeItems)
            {
                finalList += item.Node.Name;
            }
            return finalList;
        }

        /// <summary>
        /// A method to iterate a nodeItem list and print the node types, used for testing.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>

        public static string GetTypeNamesFromList(List<NodeItem> nodeList)
        {
            string result = "";
            foreach (var item in nodeList)
            {
                result += " " + item.Node.GetType().Name;
            }
            return result;
        }

        /// <summary>
        /// A method to iterate a nodeItem list and get only items with no connected children.
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>

        public static List<NodeItem> GetOutterItems(List<NodeItem> nodeItemList)
        {
            // find all nodes with no connected children
            var parents = nodeItemList.Select(x => x.ParentNode).Distinct().ToList();

            var result = new List<NodeItem>();
            foreach (var parentNode in parents)
            {
                var tempList = nodeItemList.Where(x => x.Node == parentNode);
                result.AddRange(tempList);
            }
            return result;
        }

        public static string GetTypeNamesFromNode(Node node)
        {
            var finalList = "";
            var nodeStack = new Stack<Node>();
            nodeStack.Push(node);

            while (nodeStack.Count > 0)
            {
                var nextNode = nodeStack.Pop();

                if (nextNode != null)
                {
                    foreach (var child in nextNode.GetChildrenNodes())
                    {
                        nodeStack.Push(child);
                    }
                    finalList += " " + nextNode.GetType().Name;
                }
            }
            return finalList;
        }

        /// <summary>
        /// A method to traverse the tree and create a list of NodeItems!
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static List<NodeItem> TraverseTreeDepthFirst(Node node)
        {
            var finalList = new List<NodeItem>();
            var nodeItemStack = new Stack<NodeItem>();
            nodeItemStack.Push(new NodeItem(node));

            while (nodeItemStack.Count > 0)
            {
                var nextNode = nodeItemStack.Pop();

                if (nextNode.Node != null)
                {
                    foreach (var child in nextNode.Node.GetChildrenNodes().Reverse())
                    {
                        var newNodeItem = new NodeItem(child) { ParentNode = nextNode.Node };
                        nodeItemStack.Push(newNodeItem);
                    }
                    // We only require upward traversal later, the children/subtrees are not needed.
                    nextNode.Node.RemoveChildren();
                    finalList.Add(nextNode);
                }
            }
            return finalList;
        }

        /// <summary>
        /// A method to work out and store the level of each node. Traverse the parents to find the root!
        /// </summary>
        /// <param name=""></param>
        public static void SetTreeLevels(List<NodeItem> nodeItemList)
        {
            // ignore the first item, its the root!
            for (int i = 1; i < nodeItemList.Count; i++)
            {
                var noOfSteps = 0;
                var itemToRecurse = nodeItemList[i];
                var hasParent = true;
                while (hasParent)
                {
                    itemToRecurse = GetParent(itemToRecurse, nodeItemList);
                    if (itemToRecurse.ParentNode == null)
                    {
                        hasParent = false;
                    }
                    noOfSteps++;
                }
                nodeItemList[i].TreeLevel = noOfSteps;
            }
        }

        private static NodeItem GetParent(NodeItem childNodeItem, List<NodeItem> listToSearch)
        {
            return listToSearch.FirstOrDefault(x => x.Node == childNodeItem.ParentNode);
        }


        public static Node BuildTree(List<NodeItem> nodeItems)
        {
            var rootNodes = new List<Node>();

            // iterate in reverse, recreates the tree perfectly!
            for (int i = nodeItems.Count - 1; i >= 0; i--)
            {
                if (nodeItems[i].ParentNode != null)
                {
                    nodeItems[i].ParentNode.AddChildNode(nodeItems[i].Node);
                }
                else
                {
                    rootNodes.Add(nodeItems[i].Node);
                }
            }
            return rootNodes.FirstOrDefault();
        }

    }
}
