<div class="WordSection1">
<h1>Node Tree Optimiser</h1>
This .NET console app is my solution for the developer test below. This project is test driven and includes a couple of integration tests using the Autofac DI container.
<br><h4>Solution Downloads</h4>
<a href = "https://gitlab.com/jeff_w123/NodeTree/repository/archive.zip?ref=master">Download Solution Code in Zip</a>
<br><a href = "https://gitlab.com/jeff_w123/NodeTree/tree/master">View Solution Files Online</a>
<br><br>
<p><strong><span style="font-size:12.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Overview</span></span></strong>
</p>
<p><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">About this test</span></span></strong>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">We have found that an applicant&rsquo;s CV is not a good indicator of programming aptitude, so the development team has designed this test as a way to assess applicants.</span></span>
</p>
<p><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">What we are looking at</span></span></strong>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">We are after the same style and quality of code as you would write at work on production code, so correctness and readability are important. At a minimum we expect unit tests (ideally written using TDD) and source code committed using Git. Each part of your implementation should be committed separately, resulting in a sensible commit history with detailed commit messages.</span></span>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">You may use any testing framework. You should supply the implementation as a Visual Studio 2015 solution which we can easily run. You can use any third party libraries you require from NuGet. You need to use C#.</span></span>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">The requirements are deliberately open ended. You should have questions about the requirements; assume a sensible answer and document the assumptions you make.</span></span>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Please try to avoid including personally identifiable information in your code so we can review your code as impartially as possible.</span></span>
</p>
<p><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">How to get the test results to us</span></span></strong>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Please create a private Git repository on bitbucket.org and push your work into this repository. Once you&rsquo;re done, add us as a user with Read permissions and email us to let us know you have completed the test.</span></span>
</p>
<p style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">We would appreciate any feedback on the test process; if you wish, please include a feedback document in the Git repository with any feedback.</span></span>
</p>
<p><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Submission checklist</span></span></strong>
</p>
<ol>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Visual Studio 2015 solution, written in C#, with unit tests, without any personally identifiable information</span></span></li>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">A document containing questions you would&rsquo;ve asked in the real world and any assumptions made.</span></span></li>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">An optional suggestions / feedback document.</span></span></li>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">All of the above committed to the private Bitbucket Git repository.</span></span></li>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Us added to the repository with Read rights.</span></span></li>
<li style="text-align:justify"><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Email sent to notify us that reviewing can commence.</span></span></li>
</ol>
<p><strong><span style="font-size:12.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">The test</span></span></strong>
</p>
<p><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Description</span></span></strong>
</p>
<p><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">The test is based on writing code to first describe and then transform a collection of nodes which are arranged in a tree. The node definitions are as follows:</span></span>
</p>

<p><img src= "images/Capture1.PNG"></img></p>
<p><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">There are three additional interfaces which you will implement as part of the test. These are:</span></span>
</p>
<img src= "images/Capture2.PNG"></img>
<p style="margin-left:.15pt"><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Part one - C# describer</span></span></strong>
</p>
<p><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">Write a describer implementing the </span></span><span style="font-size:9.5pt"><span style="font-family:&quot;Courier New&quot;">INodeDescriber</span></span><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif"> interface which will output a C# description of how to create the tree of nodes. The output should have node per line, indented with four spaces per nesting level.</span></span>
</p>
<p><img src= "images/Capture3.PNG"></img></p>

<p style="margin-left:.15pt"><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Part two - transformer</span></span></strong>
</p>
<p><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">Write a transformer which will transform a tree of nodes into a matching tree that uses the correct node types (e.g. a </span></span><span style="font-size:9.5pt"><span style="font-family:&quot;Courier New&quot;">ManyChildrenNode </span></span><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">with no children should be transformed into a</span></span><span style="font-size:9.5pt"><span style="font-family:&quot;Courier New&quot;"> NoChildNode</span></span><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">).</span></span>
</p>

<p><img src= "images/Capture4.PNG"></img></p>

<p style="margin-left:.15pt"><strong><span style="font-size:8.5pt"><span style="font-family:&quot;Arial&quot;,&quot;sans-serif&quot;">Part three - integration</span></span></strong>
</p>
<p><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">Write a class that implements the </span></span><span style="font-size:9.5pt"><span style="font-family:&quot;Courier New&quot;">INodeWriter</span></span><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif"> interface. The class should be designed to receive any </span></span><span style="font-size:9.5pt"><span style="font-family:&quot;Courier New&quot;">INodeDescriber</span></span><span style="font-size:8.5pt"><span style="font-family:Arial,sans-serif">, which is used to get a string representation of the tree of nodes. This string representation is then written to the specified file. You should provide at least one integration test using an off-the-shelf IoC / DI container.</span></span>
</p>
<p><img src= "images/Capture5.PNG"></img></p>