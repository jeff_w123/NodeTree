﻿using NodeTree.Data;
using System.Collections.Generic;
using System.Linq;

namespace NodeTree.Common
{
    public static class Utility
    {
        /// <summary>
        /// A method to traverse the tree and create a list of NodeItems!
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static List<NodeItem> TraverseTreeDepthFirst(Node node)
        {
            var finalList = new List<NodeItem>();
            var nodeItemStack = new Stack<NodeItem>();
            nodeItemStack.Push(new NodeItem(node));

            while (nodeItemStack.Count > 0)
            {
                var nextNode = nodeItemStack.Pop();

                if (nextNode.Node != null)
                {
                    foreach (var child in nextNode.Node.GetChildrenNodes().Reverse())
                    {
                        var newNodeItem = new NodeItem(child) { ParentNode = nextNode.Node };
                        nodeItemStack.Push(newNodeItem);
                    }

                    // We only require upward traversal later, the children/subtrees are not needed.
                    nextNode.Node.RemoveChildren();
                    finalList.Add(nextNode); 
                }
            }
            return finalList;
        }

    }
}
