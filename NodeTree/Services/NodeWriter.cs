﻿using NodeTree.Data;
using NodeTree.Interfaces;
using System.IO;
using System.Threading.Tasks;


namespace NodeTree.Services
{
    /// <summary>
    /// A class that transforms and writes a Node tree to file.
    /// </summary>

    public class NodeWriter : INodeWriter
    {
        public INodeDescriber Describer { get; set; }
        public INodeTransformer Transformer { get; set; }

        public NodeWriter(INodeDescriber describer, INodeTransformer transformer)
        {
            Describer = describer;
            Transformer = transformer;
        }

        public async Task WriteToFileAsync(Node node, string filePath)
        {
            var transformed = Transformer.Transform(node);
            var result = Describer.Describe(transformed);
            using (StreamWriter writer = File.CreateText(filePath))
            {
                await writer.WriteAsync(result);
            }
        }
    }
}
