﻿using NodeTree.Common;
using NodeTree.Data;
using NodeTree.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NodeTree.Services
{
    /// <summary>
    /// A class that reads a Node tree and outputs it to string.
    /// </summary>

    public class NodeDescriber : INodeDescriber
    {
        public string Describe(Node node)
        {
            // get all nodes without their subtrees
            var nodeItemList = Utility.TraverseTreeDepthFirst(node);

            // find and set each nodeItem's tree level
            SetTreeLevels(nodeItemList);

            // calculate and set the brackets for subtrees
            SetClosingBrackets(nodeItemList);

            // get each nodeItem and print from it.
            var result = new StringBuilder();
            for (int i = 0; i < nodeItemList.Count; i++)
            {
                // when the last one is reached....
                if (i == nodeItemList.Count - 1)
                {
                    result.Append(PrintNodeLine(nodeItemList[i], isLastInList: true));
                }
                else
                {
                result.Append(PrintNodeLine(nodeItemList[i]));                       
                }
            }
            return result.ToString();
        }

        protected string PrintNodeLine(NodeItem nodeItem, bool isLastInList = false)
        {
            // create indentation
            string indent = null;
            for (int i = 0; i < nodeItem.TreeLevel; i++)
            {
                indent += "    ";
            };

            var stringArray = new string[] {
                indent,
                "new ",
                nodeItem.Node.GetType().Name,
                "(\"",
                nodeItem.Node.Name,
                "\"",
                GetLineEnding(nodeItem.NoOfBrackets, isLastInList),
                "\n"
            };

            var result = new StringBuilder();
            for (int i = 0; i < stringArray.Length; i++)
            {
                result.Append(stringArray[i]);
            }
            return result.ToString();
        }
        /// <summary>
        /// A method to work out and store the level of each node. Traverse the parents to find the root!
        /// </summary>
        /// <param name=""></param>
        protected void SetTreeLevels(List<NodeItem> nodeItemList)
        {
            // ignore the first item, its the root!
            for (int i = 1; i < nodeItemList.Count; i++)
            {
                var noOfSteps = 0;
                var itemToRecurse = nodeItemList[i];
                var hasParent = true;
                while (hasParent)
                {
                    itemToRecurse = GetParent(itemToRecurse, nodeItemList);
                    if (itemToRecurse.ParentNode == null)
                    {
                        hasParent = false;
                    }
                    noOfSteps++;
                }
                nodeItemList[i].TreeLevel = noOfSteps;
            }
        }

        private static NodeItem GetParent(NodeItem childNodeItem, List<NodeItem> listToSearch)
        {
            return listToSearch.FirstOrDefault(x => x.Node == childNodeItem.ParentNode);
        }

        /// <summary>
        /// A method to workout and set a closing bracket for each subtree
        /// </summary>
        /// <param name="nodeItems"></param>
        protected void SetClosingBrackets(List<NodeItem> nodeItems)
        {
            for (int i = 0; i < nodeItems.Count; i++)
            {
                // if there are any children, then recurse to subtree
                if (nodeItems.Where(x => x.ParentNode == nodeItems[i].Node).ToList().Any())
                {
                    var itemToRecurse = nodeItems[i];
                    var hasChildren = true;
                    while (hasChildren)
                    {
                        var next = GetLastChild(itemToRecurse, nodeItems);
                        if (next == null)
                        {
                            itemToRecurse.NoOfBrackets++;
                            hasChildren = false;
                        }
                        // else recurse to the next subtree.
                        itemToRecurse = next;
                    }
                }
                else
                {
                    // this root has no children, so just add one bracket to it.
                    nodeItems[i].NoOfBrackets++;
                }
            }
        }

        protected NodeItem GetLastChild(NodeItem itemToRecurse, List<NodeItem> nodeItems)
        {
            return nodeItems.LastOrDefault(x => x.ParentNode == itemToRecurse.Node);
        }


        private string GetLineEnding(int noOfBrackets, bool isLastInList)
        {
            string result = "";
            for (int i = 0; i < noOfBrackets; i++)
            {
                result += ")";
            }

            if (isLastInList)
            {
                return result;
            }
            else
            {
                return result + ",";
            }
        }
    }
}
