﻿using NodeTree.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using NodeTree.Common;
using NodeTree.Data.Enums;
using NodeTree.Data;

namespace NodeTree.Services
{
    /// <summary>
    /// A class that transforms a node tree to one that uses the appropriate Node types.
    /// </summary>

    public class NodeTransformer : INodeTransformer
    {
        public Node Transform(Node node)
        {
            var allNodeItems = Utility.TraverseTreeDepthFirst(node);
            allNodeItems = optimiseTree(allNodeItems);
            return BuildTree(allNodeItems);
        }

        protected Node BuildTree(List<NodeItem> nodeItems)
        {
            var rootNodes = new List<Node>();

            for (int i = 0; i < nodeItems.Count; i++)
            {
                if (nodeItems[i].ParentNode != null)
                {
                    nodeItems[i].ParentNode.AddChildNode(nodeItems[i].Node);
                }
                else
                {
                    rootNodes.Add(nodeItems[i].Node);
                }
            }
            return rootNodes[0];
        }

        protected Node CreateUpdatedNode(NodeItem nodeItem, int requiredSlots)
        {
            switch (requiredSlots)
            {
                case 0:
                    return new NoChildrenNode(nodeItem.Node.Name);
                case 1:
                    return new SingleChildNode(nodeItem.Node.Name);
                case 2:
                    return new TwoChildrenNode(nodeItem.Node.Name);
                default:
                    throw new ArgumentException("No matching node type found for request!");
            }
        }

        protected List<NodeItem> optimiseTree(List<NodeItem> allNodeItems)
        {
            for (int i = 0; i < allNodeItems.Count; i++)
            {
                // find the connected children & available slots, then work out updates
                var children = allNodeItems.Where(x => x.ParentNode == allNodeItems[i].Node).ToList();
                var noOfChildSlots = GetNoOfChildSlots(allNodeItems[i].Node);

                // check that all slots are used, then update.
                if ( noOfChildSlots > children.Count() || 
                    noOfChildSlots < 3 && allNodeItems[i].Node.GetType() == typeof(ManyChildrenNode))
                {
                    var newNode = CreateUpdatedNode(allNodeItems[i], children.Count());
                    allNodeItems[i].Node = newNode;
                    children.ForEach(x => x.ParentNode = newNode);
                }
            }
            return allNodeItems;
        }

        private int GetNoOfChildSlots(Node thisParent)
        {
            NodeType thisNodeType;
            Enum.TryParse(thisParent.GetType().Name, out thisNodeType);

            switch (thisNodeType)
            {
                case NodeType.NoChildrenNode:
                    return 0;

                case NodeType.SingleChildNode:
                    return 1;

                case NodeType.TwoChildrenNode:
                    return 2;

                case NodeType.ManyChildrenNode:
                    var manyChildParent = thisParent as ManyChildrenNode;
                    return manyChildParent.GetNoOfManyChildSlots();

                default:
                    throw new ArgumentException("Could not find the node type in request.");
            }
        }
    }
}
