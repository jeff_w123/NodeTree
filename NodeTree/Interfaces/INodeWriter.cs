﻿using NodeTree.Data;
using System.Threading.Tasks;

namespace NodeTree.Interfaces
{
    public interface INodeWriter
    {
        Task WriteToFileAsync(Node node, string filePath);
    }
}
