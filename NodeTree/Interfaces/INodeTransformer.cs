﻿using NodeTree.Data;

namespace NodeTree.Interfaces
{
    public interface INodeTransformer
    {
        Node Transform(Node node);
    }
}
