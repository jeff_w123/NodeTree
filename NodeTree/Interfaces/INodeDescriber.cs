﻿using NodeTree.Data;

namespace NodeTree.Interfaces
{
    public interface INodeDescriber
    {
        string Describe(Node node);
    }
}
