﻿using System.Collections.Generic;

namespace NodeTree.Data
{
    public class SingleChildNode : Node
    {
        /// <summary>
        /// A derived class to represent a Node with just one child.
        /// </summary>

        public Node Child { get; private set; }
        public SingleChildNode(string name, Node child) : base(name)
        {
            Child = child;
        }

        // ctor overload
        public SingleChildNode(string name) : base(name)
        {
        }

        public override IEnumerable<Node> GetChildrenNodes()
        {
            IEnumerable<Node> children = new List<Node>() { Child };
            return children;
        }

        public override void RemoveChildren()
        {
            Child = null;
        }

        public override void AddChildNode(Node child)
        {
            Child = child;
        }
    }
}
