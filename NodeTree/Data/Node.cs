﻿using System.Collections.Generic;

namespace NodeTree.Data
{
    /// <summary>
    /// An abstract base class to represent a Node
    /// </summary>

    public abstract class Node
    {
        public string Name { get; }
        abstract public IEnumerable<Node> GetChildrenNodes();
        abstract public void RemoveChildren();
        abstract public void AddChildNode(Node child);
        protected Node(string name)
        {
            Name = name;
        }
    }
}
