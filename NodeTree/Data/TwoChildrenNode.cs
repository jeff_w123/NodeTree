﻿using System.Collections.Generic;

namespace NodeTree.Data
{
    public class TwoChildrenNode : Node
    {
        /// <summary>
        /// A derived class to represent a Node with 2 children.
        /// </summary>

        public Node FirstChild { get; private set; }
        public Node SecondChild { get; private set; }

        public TwoChildrenNode(string name, Node first, Node second) : base(name)
        {
            FirstChild = first;
            SecondChild = second;
        }

        // ctor overload
        public TwoChildrenNode(string name) : base(name)
        {
        }

        public override IEnumerable<Node> GetChildrenNodes()
        {
            IEnumerable<Node> children = new List<Node>() { FirstChild, SecondChild };
            return children;
        }

        public override void RemoveChildren()
        {
            FirstChild = null;
            SecondChild = null;
        }

        public override void AddChildNode(Node child)
        {
            if (FirstChild == null)
            {
                FirstChild = child;
            }
            else
            {
                SecondChild = child;
            }
        }
    }
}
