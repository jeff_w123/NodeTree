﻿using System.Collections.Generic;
using System.Linq;

namespace NodeTree.Data
{
    /// <summary>
    /// A derived class to represent a Node with many children.
    /// </summary>

    public class ManyChildrenNode : Node
    {
        // no of slots must be stored for when tree is flattened.
        private int _noOfChildSlots;

        public IEnumerable<Node> Children { get; private set;  }

        public ManyChildrenNode(string name, params Node[] children) : base(name)
        {
            Children = children;
            _noOfChildSlots = Children.Count();
        }

        public override IEnumerable<Node> GetChildrenNodes()
        {
            return Children;
        }

        public override void RemoveChildren()
        {
            var tempList = Children.ToList();
            for (int i = 0; i < tempList.Count; i++)
            {
                tempList[i] = null;
            }
            Children = tempList;
        }

        public override void AddChildNode(Node child)
        {
            var tempList = Children.ToList();
            for (int i = 0; i < tempList.Count; i++)
            {
                if (tempList[i] == null)
                {
                    tempList[i] = child;
                    break;
                }
            }
            Children = tempList;
        }

        public int GetNoOfManyChildSlots()
        {
            return _noOfChildSlots;
        }
    }
}
