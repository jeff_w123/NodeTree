﻿using System.Collections.Generic;
using System.Linq;

namespace NodeTree.Data
{
    /// <summary>
    /// A derived class to represent a Node without children.
    /// </summary>

    public class NoChildrenNode : Node
    {
        public NoChildrenNode(string name) : base(name)
        {
        }

        public override void AddChildNode(Node child)
        {
            // NOP
        }

        public override IEnumerable<Node> GetChildrenNodes()
        {
            return Enumerable.Empty<Node>();
        }

        public override void RemoveChildren()
        {
            // NOP
        }
    }
}
