﻿namespace NodeTree.Data.Enums
{
    /// <summary>
    /// A list of node types used for doing comparisons in the transformer class.
    /// </summary>

    public enum NodeType
    {
        NoChildrenNode,
        SingleChildNode,
        TwoChildrenNode,
        ManyChildrenNode
    }
}
