﻿namespace NodeTree.Data
{
    /// <summary>
    /// A class to store a Node, it's parent, it's tree level and an IsLeaf property
    /// </summary>
    public class NodeItem
    {
        public Node Node { get; set; }
        public Node ParentNode { get; set; }
        public int TreeLevel { get; set; }
        public int NoOfBrackets { get; set; }

        public NodeItem(Node node)
        {
            // set a default of zero
            NoOfBrackets = 0;
            Node = node;
        }
    }
}
