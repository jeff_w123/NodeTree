﻿using Autofac;
using NodeTree.Data;
using NodeTree.Interfaces;
using NodeTree.Services;
using System;

namespace NodeTree
{
    /// <summary>
    /// Program entry point
    /// </summary>
    public class Program
    {
        private static IContainer Container { get; set; }

        static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterType<NodeTransformer>().As<INodeTransformer>();
            builder.RegisterType<NodeWriter>().As<INodeWriter>();
            builder.RegisterType<NodeDescriber>().As<INodeDescriber>();
            Container = builder.Build();

            using (var scope = Container.BeginLifetimeScope())
            {
                var writer = scope.Resolve<INodeWriter>();

                var testData = new SingleChildNode("root",
                    new TwoChildrenNode("child1",
                        new ManyChildrenNode("leaf1"),
                        new SingleChildNode("child2",
                            new SingleChildNode("leaf2"))));

                Console.WriteLine("Welcome to Node Tree! \n\n" +
                    "This application takes a node, transforms it and writes it to file.\n\n"+
                    "The transformed tree will be in a file named TestFile.txt.\n\n"+
                    "Press any key to write the file and close this window....\n\n"+
                    "Thank you for taking the time to review this app!");

                Console.ReadLine();

                writer.WriteToFileAsync(testData, "TestFile.txt");
            }
        }
    }
}